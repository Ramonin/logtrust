package add;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Ramón Invarato Menéndez on 29/01/2016.
 */
public class ThreadByDivideAndConquer implements Callable<Double>{

    private List<Double> list = null;

    public ThreadByDivideAndConquer(List<Double> list) {
        this.list = list;
    }

    @Override
    public Double call() throws Exception {
        return AddCollection.addByDivideAndConquerWithThreads(list);
    }

}
