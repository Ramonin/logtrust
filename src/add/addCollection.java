package add;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Created by Ramón Invarato Menéndez on 29/01/2016.
 */
public class AddCollection {

    /**
     * Suma simple en un bucle
     *
     * @param list
     * @return
     */
    public Double addByFor(List<Double> list) {
        double result = 0d;
        for (double element : list) {
            result += element;
        }
        return result;
    }

    /**
     * Suma utilizando el último elemento de una lista que se reduce
     *
     * @param list
     * @return
     */
    public Double addByListReduction(List<Double> list) {
        while (list.size() > 1) {
            int size = list.size();
            list.set(size - 2, list.get(size - 2) + list.get(size - 1));
            list.remove(size - 1);
        }
        return list.get(0);
    }

    /**
     * Suma con cursores que parten de los extremos del listado y se juntan
     *
     * @param list
     * @return
     */
    public Double addByCursors(List<Double> list) {
        int cursorStart = 0;
        int cursorEnd = list.size() - 1;

        double result = 0d;
        while (cursorEnd >= cursorStart) {
            double last = (cursorEnd == cursorStart) ? 0 : list.get(cursorEnd--);
            double first = list.get(cursorStart++);
            result += first + last;
        }
        return result;
    }

    /**
     * Suma por unión en el centro
     *
     * @param list
     * @return
     */
    public Double addByMerge(List<Double> list) {
        while (list.size() > 1) {
            int lenght = list.size();
            int center = (lenght - 1) / 2;

            if (lenght >= 3) {
                list.set(center, list.get(center - 1) + list.get(center) + list.get(center + 1));
                list.remove(center + 1);
                list.remove(center - 1);
            } else if (lenght >= 2) {
                list.set(center, list.get(center - 1) + list.get(center));
                list.remove(center - 1);
            }
        }
        return list.get(0);
    }

    /**
     * Suma por recursión
     *
     * @param list
     * @return
     */
    public Double addByRecusive(List<Double> list) {
        int lenght = list.size();
        if (lenght == 1) {
            return list.get(0);
        } else {
            return list.get(0) + addByRecusive(list.subList(1, lenght));
        }
    }

    /**
     * Suma por algoritmo Divide y Vencerás
     *
     * @param list
     * @return
     */
    public Double addByDivideAndConquer(List<Double> list) {
        int lenght = list.size();
        if (lenght == 1) {
            return list.get(0);
        } else {
            return addByRecusive(list.subList(0, lenght / 2)) + addByRecusive(list.subList(lenght / 2, lenght));
        }
    }

    /**
     * Suma por apertura en árbol recursiva
     *
     * @param list
     * @return
     */
    public Double addByMergeRecusive(List<Double> list) {
        int lenght = list.size();
        int center = (lenght - 1) / 2;

        double middleValue = list.get(center);

        if (lenght >= 3) {
            return addByMergeRecusive(list.subList(0, center)) + middleValue + addByMergeRecusive(list.subList(center + 1, lenght));
        } else if (lenght >= 2) {
            return addByMergeRecusive(list.subList(0, center - 1)) + middleValue;
        }else{
            return middleValue;
        }
    }

    /**
     * Suma con Stream de Java 8
     *
     * @param list
     * @return
     */
    public Double addByStream(List<Double> list) {
        return list.stream().mapToDouble(i -> i.doubleValue()).sum();
    }

    /**
     * Suma con hilos simples
     *
     * @param list
     * @return
     */
    public Double addWithThread(final List<Double> list) {
        //Creo un par de Task a modo de ejemplo a los que divido los datos
        int lenght = list.size();

        Callable<Double> taskOne = () -> {
            List<Double> subListOne = list.subList(0, lenght / 2);
            return subListOne.stream().mapToDouble(i -> i.doubleValue()).sum();
        };

        Callable<Double> tastTwo = () -> {
            List<Double> subListTwo = list.subList(lenght / 2, lenght);
            return subListTwo.stream().mapToDouble(i -> i.doubleValue()).sum();
        };

        //Creo una lista de Threads a ejecutar
        List<Callable<Double>> callables = Arrays.asList(
                taskOne,
                tastTwo
        );

        //Ejecuto la lista de Threads y recojo los resultados
        List<Double> processResults = null;
        ExecutorService executor = Executors.newWorkStealingPool();
        try {
            processResults = executor.invokeAll(callables).stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    }).collect(Collectors.toList());

            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            //Tratar los errores
        } finally {
            executor.shutdownNow();
        }

        //Reutilizo el método de suma para sumar los resultados procesados
        return addByStream(processResults);
    }

    /**
     * Suma con algoritmo Divide y Vencerás con hilos
     *
     * @param list
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static Double addByDivideAndConquerWithThreads(List<Double> list) throws ExecutionException, InterruptedException {
        int lenght = list.size();
        if (lenght == 1) {
            return list.get(0);
        } else {
            ExecutorService executor = Executors.newWorkStealingPool();

            List<Double> subListOne = list.subList(0, lenght / 2);
            Future<Double> futureOne = executor.submit(new ThreadByDivideAndConquer(subListOne));

            List<Double> subListTwo = list.subList(lenght / 2, lenght);
            Future<Double> futureTwo = executor.submit(new ThreadByDivideAndConquer(subListTwo));

            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);

            return futureOne.get() + futureTwo.get();
        }
    }


}
