package log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Ramón Invarato Menéndez on 30/01/2016.
 */
public class PropertiesFileReader {

    private final String PROPERTIES_FILE = "resources/config.properties";

    private static PropertiesFileReader instance = null;

    Properties prop = null;

    private PropertiesFileReader() {
        prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        InputStream fileStream = loader.getResourceAsStream(PROPERTIES_FILE);

        try {
            InputStream fileStream = new FileInputStream(new File(PROPERTIES_FILE));
            prop.load(fileStream);
        } catch (IOException e) {
            System.err.format("%s Error%n", e);
        }
    }

    /**
     * Patrón Singleton. Crea la instancia de la clase sino existe
     *
     * @return
     */
    public static PropertiesFileReader getInstance() {
        if (instance == null) {
            instance = new PropertiesFileReader();
        }
        return instance;
    }

    /**
     * Obtener el nombre de la propiedad
     *
     * @param propertyName
     * @return
     */
    public String getProperty(String propertyName) {
        return prop.getProperty(propertyName);
    }

}
