package log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by invar on 30/01/2016.
 */
public class ReadLogFile extends Thread {


    //TODO Eliminar elementos de más de una hora de antiguedad


    private final PropertiesFileReader pfr = PropertiesFileReader.getInstance();
    private final String pathToLogsFiles = pfr.getProperty("path");
    private final String lofFileName = pfr.getProperty("logfile");
    private final String mPathToLogFile = pathToLogsFiles + lofFileName;


    //El Timestamp actúa de Identificador y sirve para borrar de memoria los registros de más de una hora
    //Map con todos los objetos Pojo. Y dos maps relacionados con el Identificador para accelerar las búsquedas
    private static ConcurrentHashMap<Long, ConnectionPojo> mListOfConnections = null;
    private static ConcurrentHashMap<String, List<Long>> mSourceToDestination = null;
    private static ConcurrentHashMap<String, List<Long>> mDestinationFromSource = null;

    private final Pattern patron = Pattern.compile("(\\d+) (\\w+) (\\w+)");

    public ReadLogFile() {
        mListOfConnections = new ConcurrentHashMap<Long, ConnectionPojo>();
        mSourceToDestination = new ConcurrentHashMap<String, List<Long>>();
        mDestinationFromSource = new ConcurrentHashMap<String, List<Long>>();
    }

    @Override
    public void run() {

        while (true) {
            if (!readLine()) {
                //Sino hay líneas nuevas que leer, el hilo espera un tiempo aleatorio antes de volver a intentar leer de fichero. Lo ideal sería un backoff exponencial
                int min = 0;
                int max = 5000;
                int randomNum = new Random().nextInt((max - min) + 1) + min;
                try {
                    TimeUnit.MILLISECONDS.sleep(randomNum);
                } catch (InterruptedException e) {
                    System.err.format("%s Error%n", e);
                }
            }
        }

    }

    private long charsReadedFromFile = 0;

    /**
     * Lee una línea del fichero
     *
     * @return devuelve true si ha leido una línea nueva, false en caso contrario
     */
    private boolean readLine() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(mPathToLogFile));
            //Continuamos desde donde dejamos la última lectura del fichero
            br.skip(charsReadedFromFile);
            if (charsReadedFromFile > 0) {
                br.readLine();
            }
            String line = br.readLine();
            br.close();

            if (line == null || line.isEmpty()) {
                return false;
            } else {
                System.out.println(" Fichero (" + mPathToLogFile + ") => " + line);

                charsReadedFromFile += line.length();
                mapNewValue(line);

                return true;
            }
        } catch (IOException e) {
            System.err.format("%s Error%n", e);
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e1) {
                    System.err.format("%s Error%n", e1);
                }
            }
            return false;
        }
    }

    /**
     * La línea leida de fichero de formato "<Long> <String> <String>" la mapea
     *
     * @param line
     */
    private void mapNewValue(String line) {

        //Separo el contenido de cada línea de fichero
        Matcher matcher = patron.matcher(line);
        matcher.find();
        String timeStamp = matcher.group(1);
        String sourceHost = matcher.group(2);
        String destinationHost = matcher.group(3);

        //Obtengo el tiempo actual que utilizaré como Id para cada inserción
        Date date = new Date();
        long currentTime = date.getTime();

        //Creo un objeto Pojo para el histórico
        ConnectionPojo cp = new ConnectionPojo();
        cp.setId(currentTime);
        cp.setSourceHost(sourceHost);
        cp.setDestinationHost(destinationHost);
        cp.setTimestamp(Long.parseLong(timeStamp));

        //Guardo el objeto Connection
        mListOfConnections.put(currentTime, cp);

        //Guardo un HashMap para acelerar las búsquedas de Origen
        if (!mSourceToDestination.containsKey(sourceHost)) {
            new ArrayList<Long>();
            mSourceToDestination.put(sourceHost, new ArrayList<Long>());
        }
        List<Long> souToDestList = mSourceToDestination.get(sourceHost);
        souToDestList.add(currentTime);

        //Guardo un HashMap para acelerar las búsquedas de Destino
        if (!mDestinationFromSource.containsKey(destinationHost)) {
            new ArrayList<Long>();
            mDestinationFromSource.put(destinationHost, new ArrayList<Long>());
        }
        List<Long> destFromSouList = mDestinationFromSource.get(destinationHost);
        destFromSouList.add(currentTime);

    }

    /**
     * Elimina los registros de la última hora
     *
     * @implNote Incompleto
     */
    public synchronized static void removeOldEntriesIfNecesary() {
        //Obtener 1 hora antes de la fecha actual
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -1);

        long lastEntryAllowed = cal.getTime().getTime();

        List<Long> oldKeys = mListOfConnections.searchKeys(mListOfConnections.size(), id -> {
            List<Long> list = new ArrayList<Long>();
            if (id < lastEntryAllowed) {
                list.add(id);
                return list;
            } else {
                return null;
            }
        });

        if (oldKeys != null) {
            for (long id : oldKeys) {
                mListOfConnections.remove(id);

//TODO            mSourceToDestination = null;
//TODO            mDestinationFromSource = null;
            }
        }

    }


    /**
     * Muestro por pantalla la lista de servidores conectados a
     *
     * @param hostName
     */
    public synchronized static void showHostNamesConnectedTo(String hostName) {
        System.out.format("%n%n%n========== Hostnames connected to a given '%s' ===========", hostName);

        mSourceToDestination.get(hostName).stream().forEach(id -> {
            String host = mListOfConnections.get(id).getDestinationHost();
            long timestamp = mListOfConnections.get(id).getTimestamp();
            System.out.format("%n <- %s [%s]", host, new Date(timestamp));
        });
    }

    /**
     * Muestro por pantalla la lista de servidores que han recibido una conexión de
     *
     * @param hostName
     */
    public synchronized static void showHostNamesReceivedConnectionsTo(String hostName) {
        System.out.format("%n%n%n========== hostnames received connections from '%s' ===========", hostName);

        mDestinationFromSource.get(hostName).stream().forEach(id -> {
            String host = mListOfConnections.get(id).getDestinationHost();
            long timestamp = mListOfConnections.get(id).getTimestamp();
            System.out.format("%n -> %s [%s]", host, new Date(timestamp));
        });
    }

    /**
     * Muestro por pantalla el servidor que más conexiones ha generado
     */
    public synchronized static void showHostNameMostConnectionsGenerated() {
        System.out.format("%n%n%n========== hostname that generated most connections in the last hour ===========");

        Map.Entry<String, List<Long>> maxConnectionEntry = null;
        long maxContConnections = -1;
        for (Map.Entry<String, List<Long>> entry : mSourceToDestination.entrySet()) {
            long contConnections = entry.getValue().size();

            if (maxContConnections < contConnections) {
                maxContConnections = contConnections;
                maxConnectionEntry = entry;
            }
        }

        System.out.format("%n  '%s' with %d connections %n%n%n", maxConnectionEntry.getKey(), maxContConnections);
    }

    /**
     * Muestra por pantalla los registros de la última hora
     *
     * @param hostRecivedConnection
     * @param hostSendConnection
     */
    public static void showLastHourAndDeleteOldEntries(String hostRecivedConnection, String hostSendConnection) {
        //Elimino entradas viejas si es necesario. Se podría programar en otro hilo que cada hora se eliminara
        removeOldEntriesIfNecesary();

        //Muestro por pantalla los últimos registros
        ReadLogFile.showHostNamesConnectedTo(hostRecivedConnection);
        ReadLogFile.showHostNamesReceivedConnectionsTo(hostSendConnection);
        ReadLogFile.showHostNameMostConnectionsGenerated();
    }

}
