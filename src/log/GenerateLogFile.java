package log;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by invar on 30/01/2016.
 */
public class GenerateLogFile extends Thread {

    private final PropertiesFileReader pfr = PropertiesFileReader.getInstance();
    private final String pathToLogsFiles = pfr.getProperty("path");
    private final String lofFileName = pfr.getProperty("logfile");
    private final String mPathToLogFile = pathToLogsFiles + lofFileName;;

    private List<String> servers = new ArrayList<String>();

    public GenerateLogFile() {
        servers.add("quark");
        servers.add("garak");
        servers.add("brunt");
        servers.add("lilac");
    }

    @Override
    public void run() {

        //Bucle de escritura en fichero
        while (true) {
            writeLineToFile();

            //Hilo espera un tiempo aleatorio antes de volver a escribir en fichero
            int min = 0;
            int max = 2000;
            int randomNum = new Random().nextInt((max - min) + 1) + min;
            try {
                TimeUnit.MILLISECONDS.sleep(randomNum);
            } catch (InterruptedException e) {
                System.err.format("%s Error%n", e);
            }
        }

    }

    /**
     * Escribe una línea generada en el fichero
     */
    private void writeLineToFile() {
        BufferedWriter bw = null;
        try {
            //Mezclar servidores
            Collections.shuffle(servers);

            //Obtener fecha actual
            Date date = new Date();
            long offset = date.getTime();

            //Obtener 5 minutos más tarde de la fecha actual
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, 5);
            long end = cal.getTime().getTime();

            //Elegir un tiempo aleatorio de entre esos cinco minutos
            long diff = end - offset + 1;
            Timestamp rand = new Timestamp(offset + (long) (Math.random() * diff));

            String line = rand.getTime() + " " + servers.get(0) + " " + servers.get(1);
            System.out.println(line + " => Fichero (" + mPathToLogFile + ")");

            //Escribir en fichero
            bw = new BufferedWriter(new FileWriter(mPathToLogFile, true));
            bw.write(line);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            System.err.format("%s Error%n", e);
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e1) {
                    System.err.format("%s Error%n", e1);
                }
            }
        }
    }

}
