package log;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ramón Invarato Menéndez on 29/01/2016.
 */
public class Main {

    private static final PropertiesFileReader pfr = PropertiesFileReader.getInstance();
    private static final String hostRecivedConnection = pfr.getProperty("hostRecivedConnection");
    private static final String hostSendConnection = pfr.getProperty("hostSendConnection");

    public static void main(String[] args) throws InterruptedException {

        //Ejecutar hilos
        ExecutorService executor = Executors.newWorkStealingPool();
        executor.submit(new GenerateLogFile());
        executor.submit(new ReadLogFile());

        //Leer comandos por consola
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            boolean isProcessing = true;
            while (isProcessing) {
                String line = in.readLine().toLowerCase();

                switch (line) {
                    case "e":
                        System.out.println("Terminando proceso...");
                        executor.shutdownNow();
                        isProcessing = false;
                        break;
                    case "p":
                        ReadLogFile.showLastHourAndDeleteOldEntries (hostRecivedConnection, hostSendConnection);
                        break;
                    default:
                        System.err.println("Comando no reconocido: " + line);
                }
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
