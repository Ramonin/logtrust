package log;

import java.security.Timestamp;

/**
 * Created by Ramón Invarato Menéndez on 31/01/2016.
 */
public class ConnectionPojo {

    private long id = 0;
    private String sourceHost = "";
    private String destinationHost = "";
    private long timestamp = 0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSourceHost() {
        return sourceHost;
    }

    public void setSourceHost(String sourceHost) {
        this.sourceHost = sourceHost;
    }

    public String getDestinationHost() {
        return destinationHost;
    }

    public void setDestinationHost(String destinationHost) {
        this.destinationHost = destinationHost;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ConnectionPojo{" +
                "id=" + id +
                ", sourceHost='" + sourceHost + '\'' +
                ", destinationHost='" + destinationHost + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

}
