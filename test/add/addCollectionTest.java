package add;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ramón Invarato Menéndez on 29/01/2016.
 */
public class AddCollectionTest {

    List<Double> list = new ArrayList<Double>();
    AddCollection ac = new AddCollection();

    private final double RESULT_TO_COMPARE = -31.6d;
    private final double DELTA = 0.0001d;

    @Before
    public void setUp() throws Exception {
        list.add(7.3d);
        list.add(-15.4d);
        list.add(2.3d);
        list.add(-8.5d);
        list.add(4.8d);
        list.add(-23.4d);
        list.add(1.3d);
    }

    @After
    public void tearDown() throws Exception {
        ac = new AddCollection();
    }

    @Test
    public void testAddByFor() throws Exception {
        double newResult = ac.addByFor(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByCursors() throws Exception {
        double newResult = ac.addByCursors(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByListReduction() throws Exception {
        double newResult = ac.addByListReduction(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByMerge() throws Exception {
        double newResult = ac.addByMerge(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByRecusive() throws Exception {
        double newResult = ac.addByRecusive(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByMergeRecusive() throws Exception {
        double newResult = ac.addByMergeRecusive(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByDivideAndConquer() throws Exception {
        double newResult = ac.addByDivideAndConquer(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByStream() throws Exception {
        double newResult = ac.addByStream(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddWithThread() throws Exception {
        double newResult = ac.addWithThread(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

    @Test
    public void testAddByDivideAndConquerWithThreads() throws Exception {
        double newResult = ac.addByDivideAndConquerWithThreads(list);
        Assert.assertEquals(RESULT_TO_COMPARE, newResult, DELTA);
    }

}