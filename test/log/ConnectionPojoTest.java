package log;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by invar on 31/01/2016.
 */
public class ConnectionPojoTest {

    private ConnectionPojo cp;

    @Before
    public void setUp() throws Exception {
        cp = new ConnectionPojo();
    }

    @Test
    public void testGetId() throws Exception {
        cp.setId(123l);
        Assert.assertEquals(123l, cp.getId());
    }

    @Test
    public void testGetSourceHost() throws Exception {
        cp.setSourceHost("abc");
        Assert.assertEquals("abc", cp.getSourceHost());
    }

    @Test
    public void testGetDestinationHost() throws Exception {
        cp.setDestinationHost("abc");
        Assert.assertEquals("abc", cp.getDestinationHost());
    }

    @Test
    public void testGetTimestamp() throws Exception {
        cp.setTimestamp(123l);
        Assert.assertEquals(123l, cp.getTimestamp());
    }

}