package log;

import add.AddCollection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by invar on 30/01/2016.
 */
public class PropertiesFileReaderTest {

    private PropertiesFileReader pfr;

    @Before
    public void setUp() throws Exception {
        pfr = PropertiesFileReader.getInstance();
    }

    @Test
    public void testGetInstance() throws Exception {
        Assert.assertNotNull(pfr);
    }

    @Test
    public void testRead() throws Exception {
        String protertyValue = pfr.getProperty("test");
        Assert.assertNotNull(protertyValue);
        Assert.assertEquals("testValue", protertyValue);
    }

}